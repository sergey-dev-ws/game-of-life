﻿using System;
using System.Diagnostics;
using System.Windows.Threading;
using GameOfLife.Models;

namespace GameOfLife.Utility
{
    public class GameOfLifeLogic
    {
        private static MatrixPopulation currentPopulation;

        private const int GenerationPeriod = 50;

        private static DispatcherTimer timer;

        private static void UpdateGeneration(object sender, EventArgs e)
        {
            Stopwatch sw = Stopwatch.StartNew();
            MatrixPopulation newPopulation = MakeNewGeneration();
            var el1 = sw.Elapsed;
            currentPopulation.SetNewPopulation(newPopulation);
            sw.Stop();
            var el = sw.Elapsed;
        }

        static GameOfLifeLogic()
        {
            timer = new DispatcherTimer();
            timer.Interval = new TimeSpan(0, 0, 0, 0, GenerationPeriod);
            timer.Tick += UpdateGeneration;
        }

        public static void Start(ref MatrixPopulation cells)
        {
            currentPopulation = cells;
            UpdateGeneration(null, null);
            timer.Start();
        }

        public static void Pause()
        {
            timer.Stop();
        }

        private static MatrixPopulation MakeNewGeneration()
        {
            var newPopulation = new MatrixPopulation(currentPopulation.RowsCount, currentPopulation.ColumnsCount);
            int rowsCount = currentPopulation.RowsCount;
            int columnsCount = currentPopulation.ColumnsCount;
            for (int i = 0; i < rowsCount; i++)
            {
                for (int j = 0; j < columnsCount; j++)
                {
                    if (currentPopulation[i, j] == 1)
                    {
                        CheckCanContinueLiving(i, j, ref newPopulation);
                    }
                    else
                    {
                        CheckCanLifeArise(i, j, ref newPopulation);
                    }
                }
            }
            return newPopulation;
        }

        private static void CheckCanContinueLiving(int row, int column, ref MatrixPopulation newPopulation)
        {
            if (HasTwoOrThreeLiveNeighbors(row, column))
            {
                newPopulation[row, column] = 1;
            }
        }

        private static void CheckCanLifeArise(int row, int column, ref MatrixPopulation newPopulation)
        {
            if (HasThreeLiveNeighbors(row, column))
            {
                newPopulation[row, column] = 1;
            }
        }

        private static bool HasTwoOrThreeLiveNeighbors(int row, int column)
        {
            int neighborsCount = currentPopulation.NeighborsCount(row, column, 1);
            return neighborsCount == 2 || neighborsCount == 3;
        }

        private static bool HasThreeLiveNeighbors(int row, int column)
        {
            return currentPopulation.NeighborsCount(row, column, 1) == 3;
        }
    }
}
