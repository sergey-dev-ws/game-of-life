﻿using System;

namespace GameOfLife.Utility
{
    public class PopulationInittializer
    {
        public static int[,] CreateGlider()
        {
            return new int[,]
                {
                    { 0, 1, 0 },
                    { 0, 0, 1 },
                    { 1, 1, 1 }
                };
        }

        public static int[,] CreateRandomPopulation(int rows, int columns)
        {
            var random = new Random();
            var randomPopulation = new int[rows, columns];
            for (int i = 0; i < rows; i++)
            {
                for (int j = 0; j < columns; j++)
                {
                    if (IsLiveCell(random))
                    {
                        randomPopulation[i, j] = 1;
                    }
                }
            }
            return randomPopulation;
        }

        private static bool IsLiveCell(Random random)
        {
            const double liveCellChance = 0.5;
            double randomValue = random.NextDouble();
            return randomValue <= liveCellChance;
        }
    }
}
