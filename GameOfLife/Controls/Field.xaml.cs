﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Shapes;
using GameOfLife.Models;

namespace GameOfLife.Controls
{
    public partial class Field : UserControl
    {
        public delegate void OnFieldInitialized();

        private readonly Brush gridBrush = new SolidColorBrush((Color)Application.Current.Resources["PhoneForegroundColor"]);

        private readonly Brush cellBrush = new SolidColorBrush((Color)Application.Current.Resources["PhoneForegroundColor"]);

        private int RowsCount
        {
            get { return (int)Math.Floor(ActualHeight / CellSize); }
        }

        private int ColumnsCount
        {
            get { return (int)Math.Floor(ActualWidth / CellSize); }
        }


        public static readonly DependencyProperty CellSizeProperty =
            DependencyProperty.Register("CellSize", typeof(double), typeof(Field), new PropertyMetadata(20.0));

        public double CellSize
        {
            get { return (double)GetValue(CellSizeProperty); }
            set { SetValue(CellSizeProperty, value); }
        }

        public static readonly DependencyProperty CellsProperty =
            DependencyProperty.Register("Cells", typeof(MatrixPopulation), typeof(Field),
                                        new PropertyMetadata(default(MatrixPopulation), CellsChangedCallback));

        public MatrixPopulation Cells
        {
            get { return (MatrixPopulation)GetValue(CellsProperty); }
            set { SetValue(CellsProperty, value); }
        }

        public static readonly DependencyProperty InitializedCommandProperty =
            DependencyProperty.Register("InitializedCommand", typeof (ICommand), typeof (Field), new PropertyMetadata(null));

        public ICommand InitializedCommand
        {
            get { return (ICommand) GetValue(InitializedCommandProperty); }
            set { SetValue(InitializedCommandProperty, value); }
        }

        public Field()
        {
            InitializeComponent();
        }



        private void DrawCells()
        {
            Stopwatch sw = Stopwatch.StartNew();
            CleanUpCanvas();
            var el1 = sw.Elapsed;
            int rowsCount = Cells.RowsCount;
            int columnsCount = Cells.ColumnsCount;

            for (int i = 0; i < rowsCount; i++)
            {
                for (int j = 0; j < columnsCount; j++)
                {
                    if (Cells[i,j] == 1)
                    {
                        DrawLiveCell(i, j);
                    }
                }
            }
            sw.Stop();
            var el = sw.Elapsed;
        }

        private void CleanUpCanvas()
        {
            FieldCanvas.Children.Clear();
        }

        private void DrawLiveCell(int row, int column)
        {
            var ellipse = new Ellipse() {Width = CellSize, Height = CellSize, Fill = cellBrush};
            FieldCanvas.Children.Add(ellipse);
            Canvas.SetLeft(ellipse, column * CellSize);
            Canvas.SetTop(ellipse, row * CellSize);
        }

        private void DrawGrid()
        {
            DrawColumns();
            DrawRows();
        }

        private void DrawRows()
        {
            double y = 0;
            int rowLinesCount = RowsCount + 1;
            for (int i = 0; i < rowLinesCount; i++, y += CellSize)
            {
                GridCanvas.Children.Add(DrawLine(0.0, ColumnsCount * CellSize, y, y));
            }
        }

        private void DrawColumns()
        {
            double x = 0;
            int columLinesCount = ColumnsCount + 1;
            for (int i = 0; i < columLinesCount; i++, x += CellSize)
            {
                GridCanvas.Children.Add(DrawLine(x, x, 0.0, RowsCount * CellSize));
            }
        }

        private Line DrawLine(double x1, double x2, double y1, double y2)
        {
            return new Line()
                {
                    Width = ActualWidth,
                    Height = ActualHeight,
                    X1 = x1,
                    X2 = x2,
                    Y1 = y1,
                    Y2 = y2,
                    Stroke = gridBrush
                };
        }

        private static void CellsChangedCallback(DependencyObject dependencyObject, DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs)
        {
            var field = (Field)dependencyObject;
            var oldValue = dependencyPropertyChangedEventArgs.OldValue as MatrixPopulation;
            var newValue = dependencyPropertyChangedEventArgs.NewValue as MatrixPopulation;

            if (oldValue != null)
            {
                oldValue.PopulationChanged -= field.PopulationChangedCallback;
            }
            if (newValue != null)
            {
                newValue.PopulationChanged += field.PopulationChangedCallback;
            }
        }

        private void PopulationChangedCallback()
        {
            DrawCells();
        }

        private void UserControl_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            DrawGrid();
            Cells.SetRowsColumnsCount(RowsCount, ColumnsCount);
            InitializedCommand.Execute(null);
        }

        private void FieldCanvas_Tap(object sender, GestureEventArgs e)
        {
            Point cellPosition = e.GetPosition(FieldCanvas);
            int row = (int)Math.Floor(cellPosition.Y / CellSize);
            int column = (int)Math.Floor(cellPosition.X / CellSize);
            if (Cells[row, column] == 0)
            {
                Cells.SetCellType(row, column, 1);
            }
            else
            {
                Cells.SetCellType(row, column, 0);
            }
        }
    }
}
