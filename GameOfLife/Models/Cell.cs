﻿using System;

namespace GameOfLife.Models
{
    public class Cell : ICell, IEquatable<Cell>
    {
        public Cell(int row, int column, CellType type)
        {
            Row = row;
            Column = column;
            Type = type;
        }

        public int Row { get; set; }

        public int Column { get; set; }

        public CellType Type { get; set; }

        public bool Equals(Cell other)
        {
            return Row == other.Row && Column == other.Column;
        }
    }
}
