﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using GameOfLife.Resources;

namespace GameOfLife.Models
{
    public class Game
    {
        public bool IsStarted { get; set; }

        public String StartPauseButtonText
        {
            get { return IsStarted ? AppResources.PauseText : AppResources.StartText; }
        }

        public Uri StartPauseButtonIcon
        {
            get
            {
                return IsStarted
                           ? new Uri("/Assets/ApplocationBarIcons/appbar.transport.pause.rest.png", UriKind.Relative)
                           : new Uri("/Assets/ApplocationBarIcons/appbar.transport.play.rest.png", UriKind.Relative);
            }
        }
    }
}
