﻿namespace GameOfLife.Models
{
    public enum CellType
    {
        Dead,
        Live,
        Null,
    }
}
