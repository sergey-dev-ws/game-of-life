﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using GameOfLife.Annotations;

namespace GameOfLife.Models
{
    public class Population<T> :  Collection<T>, INotifyPropertyChanged, INotifyCollectionChanged where T : ICell
    {
        public int RowsCount { get; private set; }

        public int ColumnsCount { get; private set; }


        public event NotifyCollectionChangedEventHandler CollectionChanged;

        public Population()
        {
            CollectionChanged += Population_CollectionChanged;
        }

        public Population(int rowsCount, int columnsCount, Func<int, int, CellType, T> cellCreator)
            : this()
        {
            RowsCount = rowsCount;
            ColumnsCount = columnsCount;
            SeedFieldWithDeadCells(cellCreator);
        }

        private void Population_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.NewItems != null)
            {
                foreach (Object item in e.NewItems)
                {
                    (item as INotifyPropertyChanged).PropertyChanged += Cell_PropertyChanged;
                }
            }
            if (e.OldItems != null)
            {
                foreach (Object item in e.OldItems)
                {
                    (item as INotifyPropertyChanged).PropertyChanged -= Cell_PropertyChanged;
                }
            }
        }

        private void Cell_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            var a = new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset);
            //OnCollectionChanged(a);
            //CollectionChanged(this, a);
        }


        public new void Add(T cell)
        {
            if (!Items.Contains(cell))
            {
                base.Add(cell);
                CollectionChanged(this, new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
            }
        }

        public void AddRange(IEnumerable<T> cells)
        {
            Stopwatch sw = Stopwatch.StartNew();
            foreach (var cell in cells)
            {
                Add(cell);
            }
            CollectionChanged(this, new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
            sw.Stop();
            var el = sw.Elapsed;
        }

        public void SetRowsColumnsCount(int rowsCount, int columnsCount, Func<int, int, CellType, T> cellCreator)
        {
            RowsCount = rowsCount;
            ColumnsCount = columnsCount;
            SeedFieldWithDeadCells(cellCreator);
        }

        public T this[int row, int column]
        {
            get
            {
                if (row >= 0 && column >= 0)
                {
                    return Items.FirstOrDefault(cell => cell.Row == row && cell.Column == column);
                }
                return default(T);
            }
        }

        public int NeighborsCount(T cell, CellType type)
        {
            return GetNeighbors(cell).Count(neighbor => neighbor.Type == type);
        }

        public IEnumerable<T> GetNeighbors(T cell)
        {
            int row = cell.Row;
            int column = cell.Column;
            return new List<T>()
                {
                    this[row - 1, column - 1],
                    this[row - 1, column],
                    this[row - 1, column + 1],
                    this[row, column - 1],
                    this[row, column + 1],
                    this[row + 1, column - 1],
                    this[row + 1, column],
                    this[row + 1, column + 1]
                }.Where(neighbor => neighbor != null);
        }

        private void SeedFieldWithDeadCells(Func<int, int, CellType, T> cellCreator)
        {
            for (int i = 0; i < RowsCount; i++)
            {
                for (int j = 0; j < ColumnsCount; j++)
                {
                    Add(cellCreator(i, j, CellType.Dead));
                }
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
