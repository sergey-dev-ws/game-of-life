﻿using System.Collections.Generic;
using System.Linq;

namespace GameOfLife.Models
{
    public class MatrixPopulation
    {
        public int RowsCount { get; private set; }

        public int ColumnsCount { get; private set; }

        private int[,] items;

        public delegate void OnPopulationChanged();

        public event OnPopulationChanged PopulationChanged;

        public MatrixPopulation()
        {

        }

        public MatrixPopulation(int rowsCount, int columnsCount)
        {
            RowsCount = rowsCount;
            ColumnsCount = columnsCount;
            items = new int[rowsCount, columnsCount];
            SeedFieldWithDeadCells();
        }

        public void AddRange(int row, int column, int[,] newItems)
        {
            int rowsCount = newItems.GetLength(0);
            int columnsCount = newItems.GetLength(1);
            for (int sourceI = 0, destI = row; sourceI < rowsCount && destI < RowsCount; sourceI++, destI++)
            {
                for (int sourceJ = 0, destJ = column;
                     sourceJ < columnsCount && destJ < ColumnsCount;
                     sourceJ++, destJ++)
                {
                    items[destI, destJ] = newItems[sourceI, sourceJ];
                }
            }
            SetPopulationChanged();
        }

        public void SetNewPopulation(MatrixPopulation cells)
        {
            items = cells.items;
            SetPopulationChanged();
        }

        public void SetRowsColumnsCount(int rowsCount, int columnsCount)
        {
            RowsCount = rowsCount;
            ColumnsCount = columnsCount;
            items = new int[rowsCount, columnsCount];
            SeedFieldWithDeadCells();
        }

        public int this[int row, int column]
        {
            get
            {
                if (row >= 0 && column >= 0 && row < RowsCount && column < ColumnsCount)
                {
                    return items[row, column];
                }
                return -1;
            }
            set
            {
                if (row >= 0 && column >= 0 && row < RowsCount && column < ColumnsCount)
                {
                    items[row, column] = value;
                }
            }
        }

        public void SetCellType(int row, int column, int type)
        {
            this[row, column] = type;
            SetPopulationChanged();
        }

        public int NeighborsCount(int row, int column, int type)
        {
            return GetNeighbors(row,column).Count(neighbor => neighbor == type);
        }

        public IEnumerable<int> GetNeighbors(int row, int column)
        {
            return new List<int>()
                {
                    this[row - 1, column - 1],
                    this[row - 1, column],
                    this[row - 1, column + 1],
                    this[row, column - 1],
                    this[row, column + 1],
                    this[row + 1, column - 1],
                    this[row + 1, column],
                    this[row + 1, column + 1]
                }.Where(neighbor => neighbor != -1);
        }

        private void SetPopulationChanged()
        {
            if (PopulationChanged != null)
            {
                PopulationChanged();
            }
        }

        private void SeedFieldWithDeadCells()
        {
            items = new int[RowsCount, ColumnsCount];
        }
    }
}


