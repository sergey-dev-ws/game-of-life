﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;

namespace GameOfLife.Models
{
    public class Population<T> : Collection<T> where T : ICell
    {
        public int RowsCount { get; private set; }

        public int ColumnsCount { get; private set; }

        public delegate void OnPopulationChanged();

        public event OnPopulationChanged PopulationChanged;
        
        public Population()
        {
            
        }

        public Population(int rowsCount, int columnsCount, Func<int, int, CellType, T> cellCreator)
        {
            RowsCount = rowsCount;
            ColumnsCount = columnsCount;
            SeedFieldWithDeadCells(cellCreator);
        }

        public new void Add(T cell)
        {
            if (!Items.Contains(cell))
            {
                base.Add(cell);
            }
        }

        public void AddRange(IEnumerable<T> cells)
        {
            foreach (var cell in cells)
            {
                Add(cell);
            }
        }

        public void SetNewPopulation(IEnumerable<T> cells)
        {
            Clear();
            AddRange(cells);
            if (PopulationChanged != null)
            {
                PopulationChanged();
            }
        }

        public void SetRowsColumnsCount(int rowsCount, int columnsCount, Func<int, int, CellType, T> cellCreator)
        {
            RowsCount = rowsCount;
            ColumnsCount = columnsCount;
            SeedFieldWithDeadCells(cellCreator);
        }

        public T this[int row,int column]
        {
            get
            {
                if (row >= 0 && column >= 0)
                {
                    return Items.FirstOrDefault(cell => cell.Row == row && cell.Column == column);
                }
                return default(T);
            }
        }

        public void SetCellType(int row, int column, CellType type)
        {
            this[row, column].Type = type;
            PopulationChanged();
        }

        public int NeighborsCount(T cell, CellType type)
        {
            return GetNeighbors(cell).Count(neighbor => neighbor.Type == type);
        }

        public IEnumerable<T> GetNeighbors(T cell)
        {
            int row = cell.Row;
            int column = cell.Column;
            return new List<T>()
                {
                    this[row - 1, column - 1],
                    this[row - 1, column],
                    this[row - 1, column + 1],
                    this[row, column - 1],
                    this[row, column + 1],
                    this[row + 1, column - 1],
                    this[row + 1, column],
                    this[row + 1, column + 1]
                }.Where(neighbor => neighbor != null);
        }

        private void SeedFieldWithDeadCells(Func<int, int, CellType, T> cellCreator)
        {
            for (int i = 0; i < RowsCount; i++)
            {
                for (int j = 0; j < ColumnsCount; j++)
                {
                    base.Add(cellCreator(i, j, CellType.Dead));
                }
            }
        }
    }
}
