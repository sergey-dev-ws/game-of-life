﻿namespace GameOfLife.Models
{
    public interface ICell
    {
        int Row { get; set; }

        int Column { get; set; }

        CellType Type { get; set; }
    }
}
