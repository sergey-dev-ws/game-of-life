using System;
using System.Windows.Input;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GameOfLife.Models;
using GameOfLife.Utility;

namespace GameOfLife.ViewModels
{
    public class MainViewModel : ViewModelBase
    {
        private Game game;

        private MatrixPopulation cells;

        #region Properties

        public MatrixPopulation Cells
        {
            get { return cells; }
            set
            {
                cells = value;
                RaisePropertyChanged("Cells");
            }
        }

        public bool IsGameStarted
        {
            get { return game.IsStarted; }
            set
            {
                game.IsStarted = value;
                RaisePropertyChanged("IsGameStarted");
                RaisePropertyChanged("StartPauseButtonText");
                RaisePropertyChanged("StartPauseButtonIcon");
            }
        }

        public String StartPauseButtonText
        {
            get { return game.StartPauseButtonText; }
        }

        public Uri StartPauseButtonIcon
        {
            get { return game.StartPauseButtonIcon; }
        }

        #endregion


        #region Commands

        private void StartPauseExecute()
        {
            StartPauseGame();
        }

        private bool CanStartPauseExecute()
        {
            return true;
        }

        public ICommand StartPause
        {
            get { return new RelayCommand(StartPauseExecute, CanStartPauseExecute); }
        }

        private void InitializePopulationExecute()
        {
            Cells.AddRange(0, 0, PopulationInittializer.CreateGlider());
        }

        private bool CanInitializePopulationExecute()
        {
            return true;
        }

        public ICommand InitializePopulation
        {
            get { return new RelayCommand(InitializePopulationExecute, CanInitializePopulationExecute); }
        }

        private void AddGliderExecute()
        {
            Cells.AddRange(0, 0, PopulationInittializer.CreateGlider());
        }

        private bool CanAddGliderExecute()
        {
            return true;
        }

        public ICommand AddGlider
        {
            get { return new RelayCommand(AddGliderExecute, CanAddGliderExecute); }
        }

        private void AddRandomPopulationExecute()
        {
            Cells.AddRange(0, 0, PopulationInittializer.CreateRandomPopulation(Cells.RowsCount, Cells.ColumnsCount));
        }

        private bool CanAddRandomPopulationExecute()
        {
            return true;
        }

        public ICommand AddRandomPopulation
        {
            get { return new RelayCommand(AddRandomPopulationExecute, CanAddRandomPopulationExecute); }
        }

        #endregion


        public MainViewModel()
        {
            game = new Game();
            Cells = new MatrixPopulation();
        }


        #region Private Methods

        private void StartPauseGame()
        {
            if (!IsGameStarted)
            {
                IsGameStarted = true;
                GameOfLifeLogic.Start(ref cells);
            }
            else
            {
                IsGameStarted = false;
                GameOfLifeLogic.Pause();
            }
        }

        #endregion

    }
}